from flask import jsonify, request, render_template

from app.processor import WordProcessor
from app import app


class Routes:
    @staticmethod
    @app.route('/api/processing')
    def index():
        return "Hello!"

    @staticmethod
    @app.route('/api/process', methods=['GET'])
    def process():
        request_body = dict(request.get_json())
        word_list = request_body.get("word_list")
        result = WordProcessor.process(word_list)
        return jsonify(result)
