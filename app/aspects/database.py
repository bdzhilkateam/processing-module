from typing import List, Dict
from functools import wraps
from app.models import Word, Meaning, Picture
from app import db


def create_word(word_str: str) -> Word:
    word = Word(word_name=word_str)
    db.session.add(word)
    db.session.commit()
    word = Word.query.filter_by(word_name=word_str).first()
    return word


def save_to_database(func):
    @wraps(func)
    def wrapper(*args):
        result: Dict[str, Dict[str, List[str]]] = {}
        result = func(*args)
        result_keys = result.keys()

        word: Word = None
        meaning: Meaning = None
        picture: Picture = None
        for key in result_keys:
            word = create_word(key)
            for meaning in result[key]['meanings']:
                meaning = Meaning(meaning_text=meaning, word_id=word.id)
                db.session.add(meaning)
            for picture in result[key]['pictures']:
                picture = Picture(picture_path=picture, word_id=word.id)
                db.session.add(picture)
        db.session.commit()
        return result
    return wrapper


def load_from_database(func):
    @wraps(func)
    def wrapper(*args):
        result: Dict[str, Dict[str, List[str]]] = {}
        word_buffer: List[str] = []
        word_list = args[0]

        meanings_list: List[str] = []
        picture_list: List[str] = []
        for word in word_list:
            cashed_word = Word.query.filter_by(word_name=word).first()
            meanings_list = []
            picture_list = []
            if cashed_word:
                print(f"FOUND | {cashed_word.word_name}")

                for meaning in cashed_word.meanings:
                    meanings_list.append(meaning.meaning_text)

                for picture in cashed_word.picture:
                    picture_list.append(picture.picture_path)

                result.update({
                    cashed_word.word_name: {
                        "meanings": meanings_list,
                        "pictures": picture_list
                    }
                })
            else:
                word_buffer.append(word)

        func_result = func(word_buffer)
        result.update(func_result)
        return result
    return wrapper