import requests
from functools import wraps
from typing import Dict, List
import os

TOKEN = os.getenv('UNSPLASH_TOKEN')

def unsplash_search(func):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) '
                      'Gecko/20100101 Firefox/24.0'
    }
    url = 'https://dictionary.cambridge.org/dictionary/english/'

    @wraps(func)
    def wrapper(*args):
        result: Dict[str, Dict[str, List[str]]] = func(*args)
        result = func(*args)
        result_keys = result.keys()

        for key in result_keys:
            if len(result[key]["pictures"]) == 0:
                response = requests.get(f'https://api.unsplash.com/search/photos?query={key}&client_id={TOKEN}')
                result[key]["pictures"].append(response.json()["results"][0]["urls"]["raw"])
        return result
    return wrapper


