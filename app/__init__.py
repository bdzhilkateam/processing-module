from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os


app = Flask(__name__)
db: SQLAlchemy = None

app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {"pool_pre_ping": True} 
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DB_CONNECTION_URL')
db = SQLAlchemy(app)

app.app_context().push()

from app.routes import Routes
