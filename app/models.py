from app import db
from sqlalchemy.dialects.postgresql import JSON


class Word(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word_name = db.Column(db.String(100), unique=True, nullable=False)

    meanings = db.relationship('Meaning', backref='word', lazy=True)
    picture = db.relationship('Picture', backref='word', lazy=True)

    def __repr__(self):
        return f'[id={self.id} | word_name={self.word_name}]'


class Meaning(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    meaning_text = db.Column(db.String(1000), nullable=False)
    word_id = db.Column(db.Integer, db.ForeignKey('word.id'), nullable=False)

    def __repr__(self):
        return f'[id={self.id} | meaning_text={self.meaning_text}]'


class Picture(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    picture_path = db.Column(db.String(1000))
    word_id = db.Column(db.Integer, db.ForeignKey('word.id'), nullable=False)

    def __repr__(self):
        return f'[id={self.id} | picture_path={self.picture_path}]'
