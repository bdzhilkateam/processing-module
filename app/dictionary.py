import requests
from abc import ABC, abstractmethod
from typing import List
from bs4 import BeautifulSoup
from app.aspects.processing import unsplash_search


class Dictionary(ABC):
    def __init__(self, word: str):
        self._word = word

    @abstractmethod
    def load(self):
        pass

    def __enter__(self):
        self.load()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    @property
    def word(self) -> str:
        return self._word

    @property
    @abstractmethod
    def meanings(self) -> List[str]:
        pass

    @property
    @abstractmethod
    def pictures(self) -> List[str]:
        pass


class TestDictionary(Dictionary):
    @property
    def meanings(self) -> List[str]:
        return ["First meaning", "Second meaning"]

    @property
    def pictures(self) -> List[str]:
        return ["First path to picture", "Second path to picture"]

    def load(self):
        pass


class CambridgeDictionary(Dictionary):
    __headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) '
                      'Gecko/20100101 Firefox/24.0'
    }
    __dictionary_url = 'https://dictionary.cambridge.org/dictionary/english/'

    def __init__(self, word: str):
        super().__init__(word)
        self.__webpage = None

    def load(self):
        response = requests.get(
            self.__dictionary_url + self._word,
            headers=self.__headers)
        self.__webpage = BeautifulSoup(response.content, 'html.parser')

    @property
    def meanings(self) -> List[str]:
        meaning_list: List[str] = []
        element_list = self.__webpage.find_all("div", {"class": "def ddef_d db"})

        for element in element_list:
            meaning_list.append(element.text)

        return meaning_list

    @property
    def pictures(self) -> List[str]:
        picture_list: List[str] = []
        element_list = self.__webpage.find_all("amp-img", {"class": "dimg_i"})

        for element in element_list:
            picture_list.append('https://dictionary.cambridge.org' + element.get('src'))

        return picture_list
