from app.dictionary import CambridgeDictionary
from app.aspects.database import *
from app.aspects.processing import unsplash_search


class WordProcessor:
    @staticmethod
    @load_from_database
    @save_to_database
    @unsplash_search
    def process(word_list: List[str]) -> Dict[str, Dict[str, List[str]]]:
        result: Dict[str, Dict[str, List[str]]] = {}
        for word in word_list:
            with CambridgeDictionary(word) as dictionary:
                result.update({
                    word: {
                        "meanings": dictionary.meanings,
                        "pictures": dictionary.pictures
                    }
                })
        return result